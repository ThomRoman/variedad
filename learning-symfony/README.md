# **Learning Symfony**

Es un proyecto de PHP de software libre que permite crear aplicaciones y sitios web rápidos
y seguros de forma profesional. Es el padre de laravel ya que el kernel de esta utiliza mayormente las
dependencias de Symfony

Este es uno de los framework mas robusto y completo para crear aplicaciones web y serviciones como 
API's para el mundo empresarial.

**Softwares que utilizan los componentes de Symfony :**
    
- Drupal
- Joomla
- Laravel
- CiviCRM
- Google API
- Facebook API
- Composer
- phpMyAdmin

```sh
    # Instalación de apache
    $ sudo apt install apache2 # por defecto apache esta encendido
    # para saber su estado
    $ systemctl status apache2 # systemctl start|stop|restart apache2 
    # por defecto corre en el puerto 80

    # para saber los puertos en linux 
    $ sudo lsof -i -P -n | grep LISTEN # sudo lsof -i -P -n

    # Instalacion de php
    # agregar los archivos de paquetes personales para php 8
    $ sudo apt install software-properties-common
    $ sudo add-apt-repository ppa:ondrej/php
    $ sudo apt update
    $ sudo apt upgrade
    $ sudo apt install php8.0
    # instalando algunas extensiones
    $ sudo apt install php8.0-common php8.0-mysql php8.0-xml php8.0-curl php8.0-gd php8.0-imagick php8.0-cli php8.0-imap php8.0-mbstring php8.0-opcache php8.0-soap php8.0-zip -y
    # configurar apache para php 8
    $ sudo vim /etc/php/8.0/apache2/php.ini # configuración
    # reiniciar
    $ sudo systemctl restart apache2 # o sudo service apache2 restart
    
    # luego
    $ sudo bash -c 'echo "<?php phpinfo(); ?>" > /var/www/html/php-info.php'

    # instalando composer (https://getcomposer.org/download/)

    $ php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    $ php -r "if (hash_file('sha384', 'composer-setup.php') === '756890a4488ce9024fc62c56153228907f1545c228516cbf63f885e036d37e9a59d27d63f46af1d4d07ee0f76181c7d3') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    $ php composer-setup.php
    $ sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer

    # Instalacion de symfony con componer (me sale error)
    $ cd /var/www/html/
    $ sudo mkdir symfony-projects
    $ cd symfony-projects
    $ sudo composer create-project symfony/website-skeleton test-symfony # yes
    $ cd test-symfony
    $ sudo composer require symfony/apache-pack # yes 

    # instalacion de symfony con wget
    $ wget https://get.symfony.com/cli/installer -O - | bash
    # ir a ~/.profile
    # y pegar el siguiente codigo
    export PATH="$HOME/.symfony/bin:$PATH"
    # luego guardarlo y hacer el source
    $ source ~/.profile

    # crear proyecto

    # If you are building a traditional web application:
    $ symfony new --full my_project

    # If you are building a microservice, console application or API:
    $ symfony new my_project

    # iniciar servidor
    $ cd my-project/
    $ symfony server:start
    $ symfony open:local

    $ composer require make
    $ composer require doctrine/annotations
    $ symfony console make:controller MainController # php bin/console make:controller MainController1
    $ php bin/console doctrine:database:create
    # symfony utiliza el ORM doctrine, comunica la app con la bd

    # una entity es una tabla  
    $ php bin/console make:entity
    $ php bin/console doctrine:schema:update --force # crear la tabla
    $ php bin/console make:user
    
    $ php bin/console make:form UserType # los formularios terminarán en type
    # esto preguntará con que entidad(tabla) estará relacionada para recoger los datos

    $ php bin/console make:entity --regenerate # regenrá los repositorios de todas las entidades
    $ composer require knplabs/knp-paginator-bundle
    $ composer require symfony/security-bundle
    $ php bin/console make:auth # para hacer el login, este comando me generará tanto el login como el logout
    # LoginFormAuthentication
```

```ini
    ; /etc/php/8.0/apache2/php.ini
    memory_limit = 256M 
    ;upload_max_filesize = 32M
    upload_max_filesize = 64M 
    ;post_max_size = 48M
    post_max_size = 64M 
    max_execution_time = 600 
    ;max_input_vars = 3000
    max_input_vars = 5000 
    max_input_time = 1000
```

## **Requesitos**

- Apache
- Composer
- PHP
- mysql y phpmyadmin (opcional)

## **Referencias**

- https://ubunlog.com/como-comprobar-los-puertos-en-uso-en-linux/
- https://github.com/ThomRoman/Administracion-Servidores
- https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-on-ubuntu-20-04-es
- https://www.youtube.com/watch?v=MNBlVO0TdYQ
- https://www.cloudbooklet.com/how-to-install-php-8-on-ubuntu/
- https://www.youtube.com/watch?v=r-DOV7ak6f8
- https://getcomposer.org/download/
- https://symfony.com/doc/current/setup/symfony_server.html#installation