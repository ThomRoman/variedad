```sh
    $ symfony new lp3-expo
    $ composer require make
    $ composer requrie doctrine/annotations
    $ symfony server:start

    $ php bin/console make:controller DashboardController # y asi para cada controlador que tengo

```

Los archivos que solo se veran son public/

src/
    Controller/ el intermediarios
    Entity/     las tablas
    Migrations/ aquí estarán las base de datos que se creen, historias de onsultas
    Repository/ consultas que son necesarias a realizar en la bd
var/
    cache y logs del sistema

## Instalar Docker

Primero desinstalar versiones antiguas de docker
`sudo apt-get remove docker docker-engine docker.io containerd runc`

```sh
$ sudo apt-get update

$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo apt-key fingerprint 0EBFCD88
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
$ sudo docker run hello-world

# para no tener que poner sudo con docker
$ sudo groupadd docker
$ sudo gpasswd -a $USER docker
$ newgrp docker

# descargando imagen de ubuntu ; docker un -it ubuntu bash

# descargando imagen de mysql
$ docker pull mysql # docker pull mysql:5.7 
$ docker run -p 3306:3306 --name mysql-8 -v /home/thom/docker-volumnes/mysql-8:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=mysql -d mysql:latest --character-set-server=utf8 --collation-server=utf8_unicode_ci
$ docker ps
$ docker exec -it mysql-8 mysql -uroot -p

## si sales
$ docker start nombre
```


# Referencias
- https://docs.docker.com/engine/install/ubuntu/