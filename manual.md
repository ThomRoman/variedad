```sh
 $ cd ~/Downloads
 $ curl -O https://repo.anaconda.com/archive/Anaconda3-5.3.0-Linux-x86_64.sh
 $ bash file.sh
 $ source .zshrc # si no se usa zsh solo hacer un source .bashrc
 $ conda info
 $ jupyter notebook
```


https://www.sicara.ai/blog/2017-05-02-get-started-pyspark-jupyter-notebook-3-minutes

**sdkman**

- openjdk 1.8.0_256
- scala 2.12.8
- spark 2.4.5 (spark-submit --version)

